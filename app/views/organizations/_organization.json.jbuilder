json.extract! organization, :id, :title, :endpoint, :access_key, :secret_key, :created_at, :updated_at
json.url organization_url(organization, format: :json)
