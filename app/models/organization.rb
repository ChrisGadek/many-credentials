class Organization < ApplicationRecord
  has_many_attached :files, service: :amazon
end
