class CreateOrganizations < ActiveRecord::Migration[7.0]
  def change
    create_table :organizations do |t|
      t.string :title
      t.string :endpoint
      t.string :access_key
      t.string :secret_key
      t.timestamps
    end
  end
end
